<?php

namespace CFS\JSONWebTokenAuthentication\Encoder;

class Base64Encoder implements Encoder
{
    /**
     * Decodes data encoded with MIME base64
     * @param string $data
     * The encoded data.
     *
     * @return string the original data or false on failure. The returned data may be
     */
    public function encode($data)
    {
        return base64_encode($data);
    }

    /**
     * Decodes data encoded with MIME base64
     *
     * @param string $data
     * The encoded data.
     * </p>
     * @return string the original data or false on failure. The returned data may be
     */
    public function decode($data)
    {
        return base64_decode($data);
    }
}