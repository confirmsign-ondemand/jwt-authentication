<?php
namespace CFS\JSONWebTokenAuthentication\Encoder;

interface Encoder
{
    /**
     * Decodes data encoded
     *
     * @param string $data
     *
     * @return string
     */
    public function encode($data);

    /**
     * Decodes data encoded
     *
     * @param string $data
     *
     * @return string
     */
    public function decode($data);
}